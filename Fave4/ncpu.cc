#include "stdafx.h"
typedef void* opaque;
#include "semx.h"

#ifdef USE_GMP
#include <gmp.h>
#endif

// MOD:  ncpu.cc
// BY:   DCT
// REV:  5-20-2015

// Abstracted Divide & Conquer parallel compute engine.

// This version vectorizes a completely independent loop across 'ncpus' threads.
// All iterations use the same parameter set & algorithm.
// (Note: this was not the case previously in frame3's array rotation stage).

// parameter space the user must fill ...
typedef struct _Idata {
	int itotal;		// size of index space
	buffer *xbuf;	// output buffer
	
	int nx, ny, nz;
	big_t xa, ya, dx, dy;
	
#ifdef USE_GMP
	mpf_t xs, ys;	// adding xa + dx doesn't work when xa~1 and dx~1e-50 w/o GMP
#endif
	
	int ncpus;		// # cpus
	opaque Cprivate;	// DnC control info
	int precision;	// used for gmp
} Idata;

// this is called by the user, who fills I ...
void execFullSet ( Idata *I );

// set parameters & execute in 1 call ...
void makeVectorFractal ( int nx, int ny, int nz, buffer *pixel, big_t xa, big_t ya, big_t dx, big_t dy, int ncpu, opaque *Priv )
{
	Idata *I = (Idata*)(*Priv);
	if (!I) {
		*Priv = I = NNW_(1,Idata);	
#ifdef USE_GMP
		mpf_init(I->xs); mpf_init(I->ys);			// only set these once
		mpf_set_d(I->xs,xa); mpf_set_d(I->ys,ya);		// they get shifted w/ following call
#endif
	}
	I->xa = xa;
	I->ya = ya;
	I->dx = dx/(nx-1);
	I->dy = dy/(ny-1);
	I->itotal = I->ny = ny;
	I->nx = nx;
	I->nz = nz;
	I->xbuf = pixel;
	I->ncpus = MAX_(ncpu,2);
	execFullSet(I);
}

void shift_start ( big_t dx, big_t dy, opaque Priv )
{
#ifdef USE_GMP
	Idata *I = (Idata*)Priv;
	mpf_t d; mpf_init(d);
	mpf_set_d(d,dx); mpf_add(I->xs,I->xs,d);
	mpf_set_d(d,dy); mpf_add(I->ys,I->ys,d);
	mpf_clear(d);
#endif
}

// this is defined by the user ...
#define ER2 4	// escape radius^2, this is 4 for Julia set
void computeSubSet_er2 ( Idata *I, int imin, int imax )
{
	buffer *o = I->xbuf + imin*I->nx;
	for ( int j=imin; j<imax; j++ ) {
		big_t y = I->ya + j*I->dy;

		for ( int i=0; i<I->nx; i++ ) {
			big_t x = I->xa + i*I->dx;

			big_t a = x;
			big_t a2 = a*a;
			big_t b = y;
			big_t b2 = b*b;

			int n;
			for (n=0; n<I->nz-1; n++) {

				big_t temp=y+2*a*b;
				a=x+a2-b2;
				a2=a*a;
				b=temp;
				b2=b*b;
				if (a2+b2>ER2) break;

		    	}
		    	*o++ = n;
		}
	}	
}

#ifndef USE_GMP
#define computeSubSet computeSubSet_er2
big_t setPrecision( int precision, opaque Priv ) { fprintf(stderr,"GMP not implemented; check makefile.\n"); return 0; }
#else

// this must be set by user to make the calls, otherwise revert to en2
big_t setPrecision( int precision, opaque Priv ) {
	Idata *I = (Idata*)Priv;
	I->precision = precision; 
	mpf_set_default_prec(precision);
	big_t epsilon = 1 / pow((big_t)2,(big_t)0.75*precision-1);
	fprintf(stderr,"GMP: precision=%d bits, eps=%Lg\n",precision,epsilon);
	return epsilon;
}

// gmp version
void computeSubSet ( Idata *I, int imin, int imax )
{
	if (!I->precision) { computeSubSet_er2(I,imin,imax); return; }
	buffer *o = I->xbuf + imin*I->nx;
	
	mpf_t x, y, a, a2, b, b2, temp, dx, dy;
	mpf_init(x);
	mpf_init(y);
	mpf_init(a);
	mpf_init(a2);
	mpf_init(b);
	mpf_init(b2);
	mpf_init(temp);
	
	mpf_init(dx);  mpf_set_d(dx,I->dx);
	mpf_init(dy);  mpf_set_d(dy,I->dy);
		
	mpf_set_d(y,imin*I->dy);  mpf_add(y,y,I->ys);	// y = ya + imin*dy
	for ( int j=imin; j<imax; j++ ) {
		
		mpf_set(x,I->xs);
		for ( int i=0; i<I->nx; i++ ) {
			
			mpf_set(a,x);
			mpf_mul(a2,a,a);
			mpf_set(b,y);
			mpf_mul(b2,b,b);

			int n;
			for (n=0; n<I->nz-1; n++) {

				mpf_mul(temp,a,b);  mpf_add(temp,temp,temp);  mpf_add(temp,y,temp);
				mpf_add(a,x,a2);  mpf_sub(a,a,b2);
				mpf_mul(a2,a,a);
				mpf_set(b,temp);
				mpf_mul(b2,b,b);
				mpf_add(temp,a2,b2);
				mpf_sub_ui(temp,temp,4L);
				if (mpf_sgn(temp)>0) break;

		    	}
		    	*o++ = n;
			mpf_add(x,x,dx);
		}
		mpf_add(y,y,dy);
	}
	
	mpf_clear(x);
	mpf_clear(y);
	mpf_clear(a);
	mpf_clear(a2);
	mpf_clear(b);
	mpf_clear(b2);
	mpf_clear(temp);
}
#endif

// **********************************************  DON'T CHANGE  ********************************************** //

// each processor in multithread task gets it own
// specific set of parameters ... all located here:
typedef struct _blockProc {
	
	// some global info: CPU# and #elements
	int icpu;
	int npts;
	
	// this is the one and only image structure
	// we should always have P->Xsz = Im->width, for example
	Idata *I;
	
	// linked list, and go/stop semaphores
	SEM_ID incoming;
	SEM_ID processed;
	struct _blockProc *next;
	
	// pre-processor & post-processor
	
	void (*PreGlobalDnC)();
	void (*PostGlobalDnC)();
	
	// define the domain portion & processing stage to use
	int ijmin, ijmax;
	void (*DivideAndConquer)(Idata*,int,int);
	
} blockProc;

// a generic D&C processor thread
void *tDncProcessor ( void *arg )
{
	blockProc *P = (blockProc*)arg;
	while (1) {
		semTake(P->incoming);	// wait for start		
		(*P->DivideAndConquer)(P->I,P->ijmin,P->ijmax);
		semGive(P->processed);	// finished
	}
	return NULL;
}

// I has been filled by user
void execFullSet ( Idata *I )
{	
	opaque *Cprivate = &I->Cprivate;
	blockProc *Processors = (blockProc*)*Cprivate;
	blockProc *P = NULL;

	// build processor space
	if (!Processors) {

		int a, b, n2_per_cpu = I->itotal/MAX_(1,I->ncpus);
		P = Processors = NNW_(1,blockProc);
		*Cprivate = (opaque*)P;

		// init
		fprintf(stderr,"Vector processor execFullSet: %d loops, %d/thread x %d threads\n",I->itotal,n2_per_cpu,I->ncpus);
		for (a=b=0; b<I->ncpus; b++) {

			// define processing algorithm
			P->DivideAndConquer = computeSubSet;

			P->incoming  = semBCreate(0);
			P->processed = semBCreate(0);
			P->icpu = b;

			P->ijmin = a;
			P->ijmax = P->ijmin + n2_per_cpu;
			a = P->ijmax;

			if (b==I->ncpus-1) P->ijmax=I->itotal;
			tLaunch ( tDncProcessor, (void*)P );
			if (b!=I->ncpus-1) P = P->next = NNW_(1,blockProc);

		}
		fprintf(stderr,"Vector processors [%p] initialized: %d threads at %.6f (last=%d)\n",Processors,I->ncpus,doubletime(),P->ijmax-P->ijmin+1);
	}
	
	// incoming parameters
	for (P=Processors; P; P=P->next) P->I = I;
	
	// process & wait
	for (P=Processors; P; P=P->next) semGive(P->incoming);
	for (P=Processors; P; P=P->next) semTake(P->processed);	
}

