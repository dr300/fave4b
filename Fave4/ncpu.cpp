#include "ncpu.h"
#include <pthread.h>

// MOD:  ncpu.cc
// BY:   DCT
// REV:  5-20-2015

// Abstracted Divide & Conquer parallel compute engine.

cNcpu::cNcpu()
{
	Processors = NULL;
}

cNcpu::~cNcpu()
{
	for (blockProc *P=Processors; P; P=P->next) { P->kill=1; semGive(P->incoming); }
/*
	for (blockProc *P=Processors; P; ) {
		blockProc *Next = P->next;
		free(P); P=Next;
	}
	free(Processors);
*/
}

// pass all user parameters & execute calculation ...
void cNcpu::computeFractal ( int nx, int ny, int nz, buffer *pixel, big_t xa, big_t ya, big_t dx, big_t dy )
{
	i_xa = xa;
	i_ya = ya;
	i_dx = dx/(nx-1);
	i_dy = dy/(ny-1);
	i_itotal = i_ny = ny;
	i_nx = nx;
	i_nz = nz;
	i_xbuf = pixel;
	this->execFullSet();
}

// worker thread method
#define ER2 4	// escape radius^2, this is 4 for Julia set
void cNcpu::computeSubSet ( int imin, int imax )
{
	buffer *o = i_xbuf + imin*i_nx;
	for ( int j=imin; j<imax; j++ ) {
		big_t y = i_ya + j*i_dy;

		for ( int i=0; i<i_nx; i++ ) {
			big_t x = i_xa + i*i_dx;

			big_t a = x;
			big_t a2 = a*a;
			big_t b = y;
			big_t b2 = b*b;

			int n;
			for (n=0; n<i_nz-1; n++) {

				big_t temp=y+2*a*b;
				a=x+a2-b2;
				a2=a*a;
				b=temp;
				b2=b*b;
				if (a2+b2>ER2) break;

		    	}
		    	*o++ = n;
		}
	}	
}

// **********************************************  DON'T CHANGE  ********************************************** //

// a generic D&C processor thread
void* cNcpu::tDncProcessor ( void *arg )
{
	blockProc *P = (blockProc*)arg;
	while (1) {
		semTake(P->incoming);	// wait for start
		if (P->kill) break;	// kill all threads?
		(*P->DivideAndConquer)(P->ijmin,P->ijmax);
		semGive(P->processed);	// finished
	}
	return NULL;
}

// execute parameter set
void cNcpu::execFullSet()
{	
	// build processor space
	if (!Processors) {

		int a, b, n2_per_cpu = i_itotal/MAX_(1,NBCPUS);
		blockProc *P = Processors = NNW_(1,blockProc);

		// init
		fprintf(stderr,"Vector processor execFullSet: %d loops, %d/thread x %d threads\n",i_itotal,n2_per_cpu,NBCPUS);
		for (a=b=0; b<NBCPUS; b++) {

			// define processing algorithm
			P->DivideAndConquer = computeSubSet;

			P->incoming  = semBCreate(0);
			P->processed = semBCreate(0);
			P->icpu = b;

			P->ijmin = a;
			P->ijmax = P->ijmin + n2_per_cpu;
			a = P->ijmax;

			if (b==NBCPUS-1) P->ijmax=i_itotal;

//			tLaunch ( tDncProcessor, (void*)P );	
			pthread_t t;
			pthread_create(&t,NULL,tDncProcessor,(void*)P);
			
			if (b!=NBCPUS-1) P = P->next = NNW_(1,blockProc);

		}
		fprintf(stderr,"Vector processors [%p] initialized: %d threads at %.6f\n",Processors,NBCPUS,doubletime());
	}
	
	// process & wait
	for (blockProc *P=Processors; P; P=P->next) semGive(P->incoming);
	for (blockProc *P=Processors; P; P=P->next) semTake(P->processed);	
}

