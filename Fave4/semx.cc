#include "semx.h"
extern FILE* bugout;

void dprintf(char* fmt, ...); // workaround for now.

// this is required for linking to a camera library that breaks sem_wait(), otherwise don't do it
//#define NO_EINTR

// wxWidgets wrapper DCT 12/27/12 ... didn't get this to work, since I can't seem to initialze outside of MyApp
#ifdef WX_SEMAPHORES

#include "wx/wxprec.h"
#ifndef WX_PRECOMP
#include "wx/wx.h"
#endif

// use <map>
#define USE_MAP

// using Kunal's map
#ifdef USE_MAP
using namespace std;
#include <map>
map<wxSemaphore*, int> sem_counts;
#endif

#define NEW_(one) (one*)calloc(1,sizeof(one))

typedef struct _psem_t {
    wxSemaphore *sem;
    int binary;
} psem_t;

SEM_ID semCreate ( int count, int max_count )
{
    SEM_ID s = NEW_(psem_t);
	((psem_t*)s)->binary = ( max_count==1 ? 1 : 0 );
	((psem_t*)s)->sem = new wxSemaphore(count,max_count);
#ifdef USE_MAP
	sem_counts[((psem_t*)s)->sem] = count;
#endif
    return(s);
}

SEM_ID semBCreate (int count) { return semCreate(count,1); }
SEM_ID semCCreate (int count) { return semCreate(count,1000); }

// no error checking yet
//int semGive (SEM_ID s) { (((psem_t*)s)->sem)->Post(); return 0; }
//int semTake (SEM_ID s) { (((psem_t*)s)->sem)->Wait(); return 0; }

// Kunal's error checking
int semGive ( SEM_ID s ) { 
   // Post
   wxSemaphore *p = s ? s->sem : NULL;
   if( p != NULL ) {
      if( p->Post() == wxSEMA_NO_ERROR ) {
#ifdef USE_MAP
         sem_counts[p]++;
#endif
         return 0;
      } else {
         return -1;
	  }
   } else {
      return -1;
   }
}

int semTake ( SEM_ID s ) { 
   // Wait
   wxSemaphore *p = s ? s->sem : NULL;
   if( p != NULL ) {
      if( p->Wait() == wxSEMA_NO_ERROR ) {
#ifdef USE_MAP
         sem_counts[p]--;
#endif
         return 0;
      } else {
		  dprintf( "********* SEMAPHORE ERROR: semTake\n");fflush(bugout);
         return -1;
	  }
   } else {
	    dprintf( "********* SEMAPHORE ERROR: semTake given NULL\n");fflush(bugout);
      return -1;
   }
}

// unimplemented
int semClear (SEM_ID s) { return 0; }

#ifndef USE_MAP
int semCount (SEM_ID s) { return 0; }
#else

int semCount(SEM_ID s) {
	wxSemaphore *p = s ? s->sem : NULL;
   
   // Look up the SEM_ID key 
   map<wxSemaphore*, int>::iterator i = sem_counts.find(p);

   // If it points to the end, that menas it didn't find the semaphore.
   if(i == sem_counts.end()) {
      dprintf( "********** Invalid semaphore given\n"); fflush(bugout);
      return -1;
   } else {
      return i->second;
   }
}

int semDelete ( SEM_ID s ) { 
	wxSemaphore *p = s ? s->sem : NULL;
   
   if( p != NULL ) {
	   dprintf( "********* Deleting semaphore\n"); fflush(bugout);
      delete p;
      sem_counts.erase(p);
      return 0;
   } else 
      return -1;
}
#endif

#else

// DCT 20Dec2012 -- Unfortunately, sem_init() still isn't implemented on MacOSX.
// The pthread_mutex counting semapore emulation (after #else below) mostly works;
// however, in certain cases, it hangs (more recently on MacOSX, with > 4 threads).

// Consequently, we're still using the (now deprecated) MPSemaphore framework, 
// and this still works fine.

#if _OSX_SEMAPHORES

#include <Carbon/Carbon.h>

typedef struct _psem_t {
	MPSemaphoreID Q;
	int max_count;
} psem_t;

SEM_ID semCreate (int count, int max_count)
{
	SEM_ID p = (SEM_ID)calloc(1,sizeof(psem_t));
	p->Q = (MPSemaphoreID)calloc(1,sizeof(MPSemaphoreID));
	int theErr = MPCreateSemaphore(p->max_count=max_count,count,&p->Q);
	if (theErr) {
		dprintf("semCreate: fail %d\n",theErr);
		return NULL;
	}	
	return(p);
}

SEM_ID semBCreate (int count) { return semCreate(count,1); }
SEM_ID semCCreate (int count) { return semCreate(count,1000); }
int semDelete ( SEM_ID p ) { return MPDeleteSemaphore(p->Q); }
int semGive ( SEM_ID p ) { return MPSignalSemaphore(p->Q); }
int semTake ( SEM_ID p ) { return MPWaitOnSemaphore(p->Q,kDurationForever); }
int semCount ( SEM_ID p ) { return 0; }

#else

enum { SEM_NORM=0, SEM_BINARY=1 };
#define NEW_(one) (one*)calloc(1,sizeof(one))

// Note the Mac version (after #else) works fine in Linux. It was once thought that the
// Linux POSIX semaphore.h would be faster, but that hasn't proven true.

#ifdef _LINUX          // DCT mod 4/19/2009

#include <errno.h>     // Check errno for EINTR, "Interrupted System Call"
extern int errno;      // which regularly breaks the sem_wait() block.

/* VxWorks user calls to implement ... */
SEM_ID semBCreate (int count);
SEM_ID semCCreate (int count);
int semCount (SEM_ID mutex);
int semGive (SEM_ID mutex);
int semTake (SEM_ID mutex);		// wait forever only

/* ... using Linux POSIX semaphore lib */
#include <semaphore.h>

/* ... these are fundamental calls ...
int sem_init ( sem_t *sem, int pshared, unsigned int value );
int sem_post ( sem_t *sem );
int sem_wait ( sem_t *sem );
int sem_timedwait ( sem_t *sem, struct timespec *abs_time );
int sem_getvalue ( sem_t *sem, int *value );
int sem_destroy ( sem_t *sem );
*/

/* ... sem = unnamed semaphore w/ pshared=0 */
typedef struct _psem_t {
    sem_t *sem;
    int binary;
} psem_t;

SEM_ID semBCreate ( int count )
{
    SEM_ID mutex = NEW_(psem_t);

    if (count>1) count=1;  // yeah, binary
    ((psem_t*)mutex)->sem=NEW_(sem_t); 
    ((psem_t*)mutex)->binary=1;
    sem_init (((psem_t*)mutex)->sem, 0, count);
    return(mutex);
}

SEM_ID semCCreate ( int count )
{
    SEM_ID mutex = NEW_(psem_t);
    ((psem_t*)mutex)->sem=NEW_(sem_t); 
    sem_init (((psem_t*)mutex)->sem, 0, count);
    return(mutex);
}

int semCount (SEM_ID mutex)
{
    int count;
    int e = sem_getvalue (((psem_t*)mutex)->sem, &count);
    if (e<0) count=e;
    return(count);
}

int semClear (SEM_ID mutex)
{
	while (semCount(mutex)>0) sem_trywait(((psem_t*)mutex)->sem);
	return 0;
}

int semGive (SEM_ID mutex)
{
    int e = sem_post(((psem_t*)mutex)->sem);
    return(e);
}

int semTake (SEM_ID mutex)
{
#ifdef NO_EINTR
    int e = sem_wait(((psem_t*)mutex)->sem);
#else
	int e=0;
	do { e=sem_wait(((psem_t*)mutex)->sem); }
	while ((e<0) && (errno==EINTR));
#endif
    return(e);
}

#else

#include <pthread.h>

typedef struct _psem_t {
    pthread_mutex_t *lock;	// changes to count are mutex locked, obviously
    pthread_mutex_t *mutex;
    int count;
    int flags;
} psem_t;

SEM_ID semCCreate (int count)
{
	SEM_ID p = NEW_(psem_t);
	p->mutex = NEW_(pthread_mutex_t);
	p->lock  = NEW_(pthread_mutex_t);
    pthread_mutex_init(p->mutex,NULL);
    pthread_mutex_trylock(p->mutex);
	p->count=count;
	pthread_mutex_unlock(p->lock);
    return(p);
}

SEM_ID semBCreate (int count)
{
    SEM_ID p = semCCreate( count>1 ? 1 : count );
    p->flags |= SEM_BINARY;
    return p;
}

// all of these routines MUST begin by locking the count (p->lock) and
// finish by unlocking before returning.

int semTake(SEM_ID p) 
{
	int current=0;
	pthread_mutex_lock(p->lock);
	current = p->count;
	
	// if already available, simply decrement count & return
	if (current>0) {
		p->count--;	// count is locked and can't have changed!
		pthread_mutex_trylock(p->mutex);
	}
	
	// count is currently zero here. The above logic guarantees
	// that count never becomes negative.
	else {
	
		// here we may have to compete for counts ...
		do {
			pthread_mutex_unlock(p->lock);
			pthread_mutex_lock(p->mutex);
	
			// count is guaranteed to have incremented at least once, unless a successive call
			// to semTake (i.e., top of this routine) has supplanted it. In that case, re-wait:
			pthread_mutex_lock(p->lock);
		} while (p->count==0);
	
		// count is positive and is locked
		p->count--;
	}
	
	// unlock & return
	pthread_mutex_unlock(p->lock);
	return(0);
}

int semGive(SEM_ID p)
{
	pthread_mutex_lock(p->lock);
	p->count++;
	if (p->flags&SEM_BINARY && p->count>1) p->count=1;
	pthread_mutex_unlock(p->mutex);
	pthread_mutex_unlock(p->lock);
	return(0);
}

int semCount(SEM_ID p)
{
	int n=0;
	pthread_mutex_lock(p->lock);
	n = p->count;
	pthread_mutex_unlock(p->lock);
	return n;
}

int semClear(SEM_ID p) 
{
	pthread_mutex_lock(p->lock);
	pthread_mutex_trylock(p->mutex);
	p->count=0;
	pthread_mutex_unlock(p->lock);
	return(0);
}

#endif	// Linux or Mac
#endif	// OSX Semaphores

// thread launcher, used unless WX_SEMAPHORES is defined
#ifndef WX_SEMAPHORES
#include <pthread.h>
unsigned long tLaunch (void *proc(void*), void *arg) 
{ 
	pthread_t t;
	pthread_create(&t,NULL,proc,arg);
	return (unsigned long)t;
}
#endif

#endif	// wxWidgets




