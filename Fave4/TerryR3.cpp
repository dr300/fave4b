//
// Utilities
//

#include "stdafx.h"

// timing
#ifdef _MSC_VER
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

// MSVC defines this in winsock2.h!?
typedef struct timeval {
    long tv_sec;
    long tv_usec;
} timeval;

int gettimeofday(struct timeval * tp, struct timezone * tzp)
{
    // Note: some broken versions only have 8 trailing zero's, the correct epoch has 9 trailing zero's
    static const uint64_t EPOCH = ((uint64_t) 116444736000000000ULL);

    SYSTEMTIME  system_time;
    FILETIME    file_time;
    uint64_t    time;

    GetSystemTime( &system_time );
    SystemTimeToFileTime( &system_time, &file_time );
    time =  ((uint64_t)file_time.dwLowDateTime )      ;
    time += ((uint64_t)file_time.dwHighDateTime) << 32;

    tp->tv_sec  = (long) ((time - EPOCH) / 10000000L);
    tp->tv_usec = (long) (system_time.wMilliseconds * 1000);
    return 0;
}
#else
#include <time.h>
#include <sys/time.h>
#endif
double doubletime(void) { struct timeval t; gettimeofday(&t,NULL); return t.tv_sec+t.tv_usec/1.e6; }

//
// Generic utilities
//

// builds an RGB colormap, returning NCOL triplets
uint8_t *ccmap (int NCOL)
{
	int j, n2=(int)(NCOL/3.55);		// need NCOL > 3.5*n2
	float dx = (float)(255.999/n2);
  	uint8_t *o,*buf;

	o=buf=NNW_(3*NCOL+3,uint8_t);
  	for (j=0; j<n2; j++) {
    		*o++=0;
    		*o++=0;
    		*o++=(int)(dx*j);
  	}

  	for (j=0; j<n2; j++) {
    		*o++=0;
    		*o++=(int)(dx*j);
    		*o++=(int)(dx*(n2-j));
  	}

  	for (j=n2-1; j>=0; j--) {
    		*o++=(int)(dx*(n2-j));
    		*o++=(int)(dx*j);
    		*o++=0;
  	}

  	for (j=NCOL-3*n2-n2/2; j>=0; j--) {
    		*o++=255;
    		*o++=0;
    		*o++=0;
  	}

  	for (j=0; j<n2; j+=2) {
    		*o++=255;
    		*o++=(int)(dx*j);
    		*o++=(int)(dx*j);
  	}

  	return(buf);
}

// do some plotting
void plotppm4 (char *fname, int Nx, int Ny, buffer *buf, int *roi, uint8_t *output, int ncolors, uint8_t *colorMap, char *id)
{
	buffer bufmin=buf[0], bufmax=buf[0];
	for ( int i=1; i<Nx*Ny; i++ ) {
		bufmin=MIN_(bufmin,buf[i]);
		bufmax=MAX_(bufmax,buf[i]);
	}
	
	for ( int i=0; i<Nx*Ny; i++) {
		int ix = (int)((ncolors-0.5)*(buf[i]-bufmin)/(bufmax-bufmin));
		for ( int m=0; m<3; m++) output[3*i+m] = colorMap[3*ix+m];
	}
	
	if (roi) {
		unsigned char red[3] = {255,0,0};
		for (int i=roi[0], j=roi[1]; j<roi[1]+roi[3]; j++) { for (int m=0; m<3; m++) output[3*(i+j*Nx)+m]=red[m]; }
		for (int i=roi[0]+roi[2], j=roi[1]; j<roi[1]+roi[3]; j++) { for (int m=0; m<3; m++) output[3*(i+j*Nx)+m]=red[m]; }
		for (int j=roi[1], i=roi[0]; i<roi[0]+roi[2]; i++) { for (int m=0; m<3; m++) output[3*(i+j*Nx)+m]=red[m]; }
		for (int j=roi[1]+roi[3], i=roi[0]; i<roi[0]+roi[2]; i++) { for (int m=0; m<3; m++) output[3*(i+j*Nx)+m]=red[m]; }
	}
	
	FILE *fd = fopen(fname,"wb");
	fprintf(fd,"P6\n%d %d\n",Nx,Ny);
	if (id) fprintf(fd,"#%s\n",id);
	fprintf(fd,"255\n");
	fwrite(output,1,3*Nx*Ny,fd);
	fclose(fd);
}
