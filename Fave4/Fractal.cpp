/////////////////////////////////////////////////////////////////////////////
// Fractal.cpp : implementation file
// cFractal
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Fractal.h"
extern int BUG;

// define this option to use ncpu.cc vectorized fractals
//#define USE_VEC
#ifdef  USE_VEC
void makeVectorFractal( int nx, int ny, int nz, buffer *pixel, big_t xa, big_t ya, big_t dx, big_t dy, int ncpu, void **Priv );
#endif

cFractal::cFractal( int mx, int my, int mz, int ncpus )
{
	julia[0] = -0.104894;
	julia[1] = -0.928036;
	julia[2] = 50.e-6;
	julia[3] = -40.e-6;
//	big_t julia[4] = {-0.104894,-0.928036,50.e-6,-40.e-6};
	fprintf(stderr,"Julia set: %Lg %Lg %Lg %Lg\n",julia[0],julia[1],julia[2],julia[3]);
	nx=mx; ny=my; nz=mz;
	fractal = NNW_(nx*ny,buffer);
	reduce = 8;
	
	// plot
	ncolors = 256;
	colorMap = NULL;
	
	// misc
	roi = NULL;
	ncpu = ncpus;
	vecPrivate = NULL;
	
#ifdef USE_VEC
	fprintf(stderr,"Vector Processing Enabled.\n");
#else
	fprintf(stderr,"Vector Processing *not* enabled.\n");
#endif
	fprintf(stderr,"GMP Calculations are *not* enabled - use fractal3 for that.\n");
}

cFractal::~cFractal()
{
	if (fractal) free(fractal);
	if (colorMap) free(colorMap);
}

// Julia set with escape^2==4 ... non-optimized
void cFractal::fractal_f2 ( double *xymx )
{
	int n, nz1 = nz-1;
	double xa = xymx[0];
	double xb = xymx[1];
	double ya = xymx[2];
	double yb = xymx[3];
	double dx = (xb-xa)/(nx-1);
	double dy = (yb-ya)/(ny-1);
	double x=xa, y=ya, a, b, a2, b2, temp;
	buffer *o = fractal;
	double check = 0;

	if (BUG) {
		fprintf(stderr,"... fractal_f2: %dx%dx%d\n",nx,ny,nz);
		fprintf(stderr,"... buffer: %p\n",fractal);
		fprintf(stderr,"... xymx: %g %g %g %g\n",xa,xb,dx,dy);
	}

//	for (yb=ya,n=0; n<ny; n++) yb+=dy;  // compute exact bounds
//	for (xb=xa,n=0; n<nx; n++) xb+=dx;  // and avoid round-off error

//	int c = 0;
	for ( int jj=0; jj<ny; jj++, y+=dy ) {
	    x=xa;
	    for ( int ii=0; ii<nx; ii++, x+=dx ) {


//	for (y=ya; y!=yb; y+=dy) {
	// 	for (x=xa; x!=xb; x+=dx) {
	   		for (a=x,a2=a*a,b=y,b2=b*b,n=0; n<nz1; n++) {
				temp=y+2*a*b;
				a=x+a2-b2;
				a2=a*a;
				b=temp;
				b2=b*b;
				if (a2+b2 >= 4) break;
	    		}

//			c++;
//			if (c >= nx*ny) fprintf(stderr,"c=%d\n",c);

	    		*o++ = n;
			if (BUG) check += n;
		}
	}
	if (BUG) fprintf(stderr,"... fractal_f2: check = %g\n",check);
}

void cFractal::makeFastFractal()
{
	double xymx[4] = {julia[0],julia[0]+julia[2],julia[1],julia[1]+julia[3]};
	printf("... sizeof(fractal): %lu-bit\n",8*sizeof(buffer));
	fractal_f2 (xymx);
}

// more general computation
void cFractal::fractal_f3()
{
	big_t xa = julia[0];
	big_t ya = julia[1];
	big_t dx = julia[2];
	big_t dy = julia[3];
	
#ifdef USE_VEC
	if (ncpu > 1) {
		makeVectorFractal(nx,ny,nz,fractal,xa,ya,dx,dy,ncpu,&vecPrivate);
		return;
	}
#endif	
	
	dx /= (nx-1);
	dy /= (ny-1);
	
    	big_t x, y, a, b, a2, b2, temp;
    	buffer *o = fractal;

    /* Mandlebrot set:

       For each (x,y) in the defined xymx[4] map, iteratively
       examine the following complex parabolic equation set:

          	f(z) = z^2 + x + iy
	  		z[n+1] = f(z[n])
	  		z[n] = 0.

       We want to plot convergence! If |z|>2 at any time it doesn't.
       As we iterate, we'll plot the number of iterations n that it
       takes to make |z|>2. If n gets large, perhaps limited by our
       iteration ceiling of nz, we'll call it connected. If n is
       small, so that non-convergence is quickly determined, we'll 
       call it non-connected. Plotting colorcontours of n looks cool.
    */
	
	for ( int j=0; j<ny; j++ ) {
		y = ya + j*dy;
		
		for ( int i=0; i<nx; i++ ) {
			x = xa + i*dx;
			
			a=x;
			a2=a*a;
			b=y;
			b2=b*b;
			
			int n;
			for (n=0; n<nz-1; n++) {
				
				temp=y+2*a*b;
				a=x+a2-b2;
				a2=a*a;
				b=temp;
				b2=b*b;
				if (a2+b2 >= 4) break;
				
	    		}
	    		*o++ = n;
		}
	}
}

// better roundoff? I'm not entirely sure ...
void cFractal::fractal_f3b()
{
	big_t xa = julia[0];
	big_t ya = julia[1];
	big_t dx = julia[2];
	big_t dy = julia[3];
	
	dx /= (nx-1);
	dy /= (ny-1);
	
	big_t a, b, a2, b2, ab, A, B, dA, dB, dA0, dB0, thresh, idx, jdy, test;
    	buffer *o = fractal;

    /* Mandlebrot set:

       For each (x,y) in the defined xymx[4] map, iteratively
       examine the following complex parabolic equation set:

          	f(z) = z^2 + x + iy
	  		z[n+1] = f(z[n])
	  		z[n] = 0.

       We want to plot convergence! If |z|>2 at any time it doesn't.
       As we iterate, we'll plot the number of iterations n that it
       takes to make |z|>2. If n gets large, perhaps limited by our
       iteration ceiling of nz, we'll call it connected. If n is
       small, so that non-convergence is quickly determined, we'll 
       call it non-connected. Plotting colorcontours of n looks cool.
    */
	
	for ( int j=0; j<ny; j++ ) {
		jdy = j*dy;
		b=ya + jdy;
		b2=b*b;
		
		for ( int i=0; i<nx; i++ ) {
			idx = i*dx;
			a=xa + idx;
			a2=a*a;
			
			ab = a*b;
			
			A = xa + a2 - b2;
			B = ya + 2*a*b;
			thresh = 4 - A*A - B*B;
			
			dA = idx;
			dB = jdy;
			
			dA0 = b2-a2 + A*A - B*B + idx;
			dB0 = -2*a*b + 2*A*B + jdy;
			
			A *= 2;
			B *= 2;
			
			int n;
			for (n=0; n<nz-1; n++) {

				// both dA and dB are small, so ab is small
				ab = A*dB + B*dA + 2*dB*dA;
				
				// these may not be small - but dB may go static
				dA = dA0 + A*dA - B*dB + dA*dA - dB*dB;	
				dB = dB0 + ab;
				
				// convergence
				test = A*dA  + B*dB  - thresh;
				if (test >= 0) break;
				
	    		}
	    		*o++ = n;
		}
	}
}

// ROI's an nx x ny fracal down, rescaling back to nx x ny
void cFractal::getFractalRoi()
{
	memset(fractal,0,nx*ny*sizeof(buffer));
	
	if (roi) {
		julia[0] += (big_t)roi[0]/(big_t)nx*julia[2];
		julia[1] += (big_t)roi[1]/(big_t)ny*julia[3];
		julia[2] *= (big_t)roi[2]/(big_t)nx;
		julia[3] *= (big_t)roi[3]/(big_t)ny;
	}
	else
		printf("... sizeof(fractal): %lu-bit\n",MIN_(80,8*sizeof(big_t)));
	
	fractal_f3();
	
	if (BUG) {
		double check = 0;
		for ( int n=0; n<nx*ny; n++ ) check += fractal[n];
		fprintf(stderr,"julia_set: %Lg %Lg %Lg %Lg\n",julia[0],julia[1],julia[2],julia[3]);
		fprintf(stderr,"fractal_f3(%dx%dx%d): check = %g\n",nx,ny,nz,check);
	}
}

void cFractal::reduceFractal()
{
	if (!roi) roi=NNW_(4,int);
	roi[2] = nint(nx/reduce);
	roi[3] = nint(ny/reduce);
}

// wrapper if you don't want to keep the RGB output
void plotppm4 (char *fname, int Nx, int Ny, buffer *buf, int *roi, uint8_t *output, int ncolors, uint8_t *colorMap, char *id);
uint8_t *ccmap (int NCOL);
void cFractal::plotFractal( char *fname, buffer *qmap )
{
	if (!colorMap) colorMap = ccmap(ncolors);
	uint8_t *orgb = NNW_(3*nx*ny,uint8_t);
	plotppm4(fname,nx,ny,qmap ? qmap : fractal,qmap ? NULL : roi,orgb,ncolors,colorMap,NULL);
	free(orgb);
}

void cFractal::plotLogQmap( char *fname, int norders, buffer *qmap )
{
	buffer xmin=qmap[0], xmax=qmap[0];
	for ( int j=0; j<nx*ny; j++ ) {
		if (qmap[j]<xmin) xmin=qmap[j];
		else if (qmap[j]>xmax) xmax=qmap[j];
	}

	buffer *tmp = NNW_(nx*ny,buffer);

	for ( int j=0; j<nx*ny; j++ ) {
		double x = (qmap[j]-xmin)/(xmax-xmin);
		x = log10(x+1.e-38)/norders + 1;
		tmp[j] = MAX_(0,x);
	}
	
	plotFractal(fname,tmp);
	free(tmp);
}
