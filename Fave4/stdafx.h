// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//
// for Mac/Linux, we'll just use this as well

#ifdef _MSC_VER

#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include "targetver.h"
#include <tchar.h>

// Win7 command-line programs need something like this
#define _MAIN _tmain
#define _DONE { printf("Enter space to finish ...\n"); char c; fread(&c,1,1,stdin); return 0; }

#else

#pragma GCC diagnostic ignored "-Wwrite-strings"
typedef char _TCHAR;

#define _MAIN main
#define _DONE { return 0; }

#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

typedef double buffer;

//#define PI 3.14159265358979323846
const double PI	= double( 3.1415926535897932384626433832795028841971693993751058209749445923078156406286208998628034825342117067982148 );

#define NNW_(n,one) (one*)calloc(n,sizeof(one))
#define MIN_(x,y) ((x) < (y) ? ((x)) : (y))
#define MAX_(x,y) ((x) > (y) ? ((x)) : (y))
#define BRACK_(a,x,b) MIN_(b,(MAX_(a,x)))
#define nint(x) ((int)floor(x+.5))

double doubletime();

typedef long double big_t;	// this is IEEE 80-bit
