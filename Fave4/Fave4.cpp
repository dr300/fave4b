// Fave4.cpp : Defines the entry point for the console application.
//
//
// Computational image processing benchmark
// Single threaded only
//       -- computes Qmap of Fractlal, ROI's down
// DCT Nov 2015 (ported from fractal3)
//
// Fave.cpp -- compile on Mac/Linux as
//      gcc -O4 fave4.cpp -o fave4 -lm
// Also tested on Win7 with VC++2012 &/ IntelXE13.0
//
// Try this:
//      fave4 -n 8 -q 1
// And examine qmap8.pgm. It takes 42 sec to run on my iMac.
// Run the script cool.x qmap8.pgm to get colored output.
//

#include "stdafx.h"
#include "Qmap.h"
#include "Fractal.h"
int BUG = 0;

int _MAIN (int argc, _TCHAR* argv[])
{
	int xdim=11, ydim=10;
	int nx=1<<xdim, ny=1<<ydim;
	int ncpu=0;
	buffer qsize = 0;

	// number of fractals to compute
	int loops = 1;
	for ( int j=1; j<argc; j++ ) {
		if (argv[j][0]=='-') {
			if (argv[j][1]=='b') BUG++;
			else if (argv[j][1]=='n') loops = (int)atof(argv[++j]);
			else if (argv[j][1]=='q') qsize = atof(argv[++j]);
			else if (argv[j][1]=='v') ncpu = (int)atof(argv[++j]);
		}
	}
	printf("%s: %dx%d test fractal (%d) ...\n",*argv,nx,ny,loops);
	cFractal *m_pFractal = new cFractal(nx,ny,256,ncpu);
	
	// compute fractal
	double tstart = doubletime();
	if (loops>1) m_pFractal->getFractalRoi();
	else m_pFractal->makeFastFractal();
	printf("... fractal: %.6f sec\n",doubletime()-tstart);
	
	// compute Q-map of fractal
	cQmap *m_pQmap = new cQmap;
	if (qsize) m_pQmap->setKsize(qsize);
	double tmark = doubletime();
	buffer Q = m_pQmap->Qmap_pow2(m_pFractal->fractal,xdim,ydim);
	
	// ROI down
	m_pFractal->reduceFractal();
	buffer Qroi = m_pQmap->findQroi3(m_pFractal->roi);
	printf("... ROI: %dx%d (%d,%d) Qroi=%g\n",m_pFractal->roi[2],m_pFractal->roi[3],m_pFractal->roi[0],m_pFractal->roi[1],Qroi);
	
	double now = doubletime();
	printf("... Qmetric: %.6f sec\n",now-tmark);
	printf("... Q=%g;  %.3f sec\n",Q,now-tstart);
	m_pQmap->plotLogQmap("qmap.pgm",8);
	
	// output colored fractal showing roi
	m_pFractal->plotFractal("roi.ppm",NULL);
	
	// loops?
	char froi[12];
	for ( int loop=2; loop<=loops; loop++ ) {
		m_pFractal->getFractalRoi();
		Q = m_pQmap->Qmap_pow2(m_pFractal->fractal,xdim,ydim);
		Qroi = m_pQmap->findQroi3(m_pFractal->roi);
		sprintf(froi,"roi%d.ppm",loop);
		printf("... [%d] %s Qroi(%d,%d) %.3f Qroi/Q %.3f time %.3f sec\n",
				loop,froi,m_pFractal->roi[0],m_pFractal->roi[1],Qroi,Qroi/Q,doubletime()-tstart);
		m_pFractal->plotFractal(froi,NULL);
		sprintf(froi,"qmap%d.pgm",loop); m_pQmap->plotLogQmap(froi,8);
	}
	
	// create final qmap
	sprintf(froi,"qmap%d.ppm",loops);
//	m_pFractal->plotFractal(froi,m_pQmap->qmap);
	m_pFractal->plotLogQmap(froi,8,m_pQmap->qmap);
	
	// done
	delete m_pFractal;
	delete m_pQmap;
	_DONE
}

