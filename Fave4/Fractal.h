/////////////////////////////////////////////////////////////////////////////
// Fractal.cpp : implementation file
// cFractal
/////////////////////////////////////////////////////////////////////////////

#ifndef INC_FRACTAL_H
#define INC_FRACTAL_H

class cFractal
{   
	
public:
	
	cFractal(int mx, int my, int mz, int ncpus);
	virtual ~cFractal();
	
	buffer *fractal;

	void fractal_f2 ( double *xymx );
	void makeFastFractal();
	void fractal_f3();
	void fractal_f3b();
	void getFractalRoi();
	void plotFractal (char *fname, buffer *qmap);
	void plotLogQmap( char *fname, int norders, buffer *qmap );
	
	
	// roi reduction
	int *roi;
	buffer reduce;
	void reduceFractal();
	
private:
	
	int nx, ny, nz;
	big_t julia[4];
	
	// plotting
	int ncolors;
	uint8_t *colorMap;
	
	// vectors
	int ncpu;
	void *vecPrivate;

};

#endif
