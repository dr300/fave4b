/////////////////////////////////////////////////////////////////////////////
// Qmap.cpp : implementation file
// cQmap
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Qmap.h"

cQmap::cQmap()
{
	m_pFourierP2 = new cFourierP2();
	border = 4;
	bstepz = 16;
	qmap = NULL;
	work = NULL;
	kfft = NULL;
	knx = kny = 0;
	KSIZE5 = 0;
}

cQmap::~cQmap()
{
	if (qmap) free(qmap);
	if (kfft) free(kfft);
	if (work) free(work);
	delete m_pFourierP2;
}

// Computes analytic FourierP2 Transform of 2D Laplacian operator
// This is required for vo5 transform
buffer* cQmap::laplace_trig ( int nx, int ny )
{	
	buffer dx = 2*PI/nx;
	buffer *trig, *ytrig;
	buffer *xtrig = NNW_(nx+ny,buffer);
	
	// create cosine table
	for (int k=0; k<nx; k++) xtrig[k]=2*(cos(k*dx)-1);
	
	if (nx == ny) ytrig = xtrig;
	else {
		dx = 2*PI/ny;
		ytrig = xtrig + nx;
		for (int k=0; k<ny; k++) ytrig[k]=2*(cos(k*dx)-1);
	}

	trig = NNW_(nx*ny,buffer);
	for (int k=0, j=0; j<ny; j++)
		for (int i=0; i<nx; i++,k++)
			trig[k] = xtrig[i] + ytrig[j];
			
	free(xtrig);
	return(trig);
}

//  In-place complex 1D quadrant swap
void cQmap::qswitch1d (buffer *c, int n, int inc)
{
	buffer sav1[2], sav2[2];
	int n2=n/2, j1, j2, i1, i2, i3, i4;

	put2(sav1,0,c,0);
	put2(sav2,0,c,inc);

	for (int i=0; i<n2-3; i+=2) {
		j1 = (n2+i+1)*inc;
		j2 = j1+inc;
		i1 = i*inc;
		i2 = i1+inc;
		i3 = i2+inc;
		i4 = i3+inc;
		put2(c,i1,c,j1);
		put2(c,i2,c,j2);
		put2(c,j1,c,i3);
		put2(c,j2,c,i4);
	}
	i1 = (n-1)*inc;
	i2 = n2*inc;
	i3 = i2-inc;
	i4 = i3-inc;
	put2(c,i4,c,i1);
	put2(c,i1,c,i2);
	put2(c,i2,sav2,0);
	put2(c,i3,sav1,0);
}

//  In-place complex 2D quadrant swap, only works on even nx
#if 1
void cQmap::qswitch2d (buffer *c, int nx, int ny)
{
  for (int j=0; j<ny; j++) qswitch1d(&c[2*j*nx],nx,1);
  for (int j=0; j<nx; j++) qswitch1d(&c[2*j],ny,nx);
}
#else
void cQmap::qswitch2d (buffer *c, int nx, int ny)
{
	int i, j, ii, jj, ix, newx;
	int nbytes = 2*nx*ny*sizeof(buffer);
	buffer *tmp = (buffer*)malloc(nbytes);

	for (j=0; j<ny; j++) {
		jj = j + ny/2;
		if (jj>=ny) jj-=ny;
		
		for (i=0; i<nx; i++) {
			ii = i+nx/2;
			if (ii>=nx) ii-=nx;
			
			newx = ii+jj*nx;
			ix = i+j*nx;
			
			tmp[2*newx] = c[2*ix];
			tmp[2*newx+1] = c[2*ix+1];
			
		}
	}

	memcpy(c,tmp,nbytes);
	//dprintf("**************** qmetric qswitch2d free(tmp)*********\n");
	free(tmp);
}
#endif

buffer* cQmap::gaussianComplex2_pow2 ( bcomplex *kbuf, int mdim, int ndim, buffer kpixels )
{
	int nx = 1<<mdim;
	int ny = 1<<ndim;
	
	buffer x, y, xc=(nx+2)/2, yc=(ny+2)/2;
//	buffer x, y, xc=nx/2, yc=ny/2;	// bug fix DCT 5-5-2010, to conform to OOP qswitch2d
	buffer sum = 0;
	buffer *kfft = NNW_(nx*ny,buffer);

	for (int j=0; j<ny; j++) {
		y = (j-yc)/kpixels;
		for (int i=0; i<nx; i++) {
			x = (i-xc)/kpixels;
			kbuf[i+j*nx].re = exp(-x*x-y*y);
			kbuf[i+j*nx].im = 0;
			sum += kbuf[i+j*nx].re;
		}
	}

	qswitch2d( (buffer*)kbuf, nx, ny);	
	m_pFourierP2->fft248xy( (buffer*)kbuf, mdim, ndim, -1 );
	
	// bug fix Dec 2008
	for (int j=0; j<nx*ny; j++) kfft[j] = sqrt(kbuf[j].re*kbuf[j].re + kbuf[j].im*kbuf[j].im) / sum;
	return(kfft);
}

// operates on a 2^mdim x 2^mdim image ONLY
buffer cQmap::Q_pow2 ( buffer *xbuf, int mdim, int ndim )
{
	// dimensions
	int nx = 1<<mdim;
	int ny = 1<<ndim;
	int n2 = nx*ny;
	
	if (kfft && ( nx!=knx || ny!=kny )) {
		free(kfft); kfft=NULL;
		free(work); work=NULL;
	}

	if (!kfft) {
		knx=nx; kny=ny;	// internal dimensions associated with kfft
		buffer *trig = laplace_trig ( nx, ny );
		if (!KSIZE5) KSIZE5=sqrt(sqrt((double)n2))/10.4634; 
		if (work) free(work);
		work = NNW_(n2,bcomplex);
		kfft = gaussianComplex2_pow2(work,mdim,ndim,KSIZE5);		// static convolution kernel
		
		// build in laplacian
		for (int k=0, j=0; j<ny; j++)
			for (int i=0; i<nx; i++,k++) {
				kfft[k] *= trig[k];					// combine Laplacian trig table and gaussian kernel
//				kfft[k] *= kfft[k];					// use kfft[k]^2 in Vo5 multiply accumulate loop
//				kfft[k] *= n2;						// normalize for xform and average				
				kfft[k] *= sqrt((double)n2);						// normalize for xform and average				
			}
		free(trig);
	}

	// copy xbuf into bcomplex work array
	double average=0, sum2=0;
	for (int k=0; k<n2; k++) {
		average   += xbuf[k];
		sum2      += xbuf[k]*xbuf[k];
		work[k].re = xbuf[k];
		work[k].im = 0;
	}

	// compute T=FFT(UTK) where U=FFT(u), T=laplacian trig table, and K=spectral gaussian
	m_pFourierP2->fft248xy((buffer*)work,mdim,ndim,-1);
	
	double vo5=0;
	for (int k=0; k<n2; k++) {
		work[k].re *= kfft[k];
		work[k].im *= kfft[k];
		vo5 += (work[k].re*work[k].re+work[k].im*work[k].im);
	}
	
	vnorm = 64*(sum2*n2-average*average)*sqrt(640*480/(double)n2);	// Q-metric normalization
	vo5 /= vnorm;

	return(vo5);
}

buffer cQmap::Qmap_pow2 ( buffer *xbuf, int mdim, int ndim )
{
	buffer Q = Q_pow2(xbuf,mdim,ndim);
	if (!qmap) qmap=NNW_(knx*kny,buffer);
	m_pFourierP2->fft248xy((buffer*)work,mdim,ndim,1);
	for (int k=0; k<knx*kny; k++) qmap[k] = work[k].re*work[k].re / vnorm / (knx*kny);
	return(Q);
}

// computes Qmap in a defined ROI
buffer cQmap::vQsumX ( int *roi )
{	
	int width = roi ? roi[2] : knx;
	int height = roi ? roi[3] : kny;
	buffer Q=0;
	buffer *thisRow = roi ? qmap + roi[0] + roi[1]*knx : qmap;
	for ( int j=0; j<height; j++, thisRow+=knx ) {
		for ( int i=0; i<width; i++ ) {
			Q += thisRow[i];
		}
	}
	Q /= (width*height);
	return(Q);
}

// find ROI with maximum Qmap sum
// ROI size is roi[2] x roio[3] ... the upper left corner (roi[0],roi[1]) is computed
buffer cQmap::findQroi3 ( int *roi )
{
	int istep=(knx-2*border)/bstepz, jstep=(kny-2*border)/bstepz;
	
	int i0=0, j0=0;
	buffer Qmax=0;
	for (int j=border; j<kny-border; j+=jstep) {
		for (int i=border; i<knx-border; i+=istep) {
			roi[0] = i;
			roi[1] = j;
			
			if ( roi[0]+roi[2] > knx-border ) roi[0] = knx-border-roi[2];
			if ( roi[1]+roi[3] > kny-border ) roi[1] = kny-border-roi[3];
			
			buffer Q = vQsumX (roi);

			if (Q > Qmax) {
				Qmax=Q;
				i0=roi[0];
				j0=roi[1];
			}
		}
	}
	
	roi[0] = i0;
	roi[1] = j0;
	Qmax *= knx*kny;
	return(Qmax);
}

void cQmap::plotLogQmap ( char *fname, int norders )
{
	buffer xmin=qmap[0], xmax=qmap[0];
	for ( int j=0; j<knx*kny; j++ ) {
		if (qmap[j]<xmin) xmin=qmap[j];
		else if (qmap[j]>xmax) xmax=qmap[j];
	}
	FILE *fd = fopen(fname,"wb");
	fprintf(fd,"P5\n%d %d\n255\n",knx,kny);
	for ( int j=0; j<knx*kny; j++ ) {
		double x = (qmap[j]-xmin)/(xmax-xmin);
		x = log10(x+1.e-38)/norders + 1;
		uint8_t c = (uint8_t)BRACK_(0,(255.5*x),255);
		fwrite(&c,1,1,fd);
	}
	fclose(fd);
}
