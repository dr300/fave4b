/////////////////////////////////////////////////////////////////////////////
// FourierP2.cpp : implementation file
// cFourierP2
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "FourierP2.h"
extern int BUG;

/*
   MODULE: FFTLIB
   BY:     Dave Terry
   DATE:   June-1-85

   2D FFT library for powers of 2
      with unrolled loops for radix2^1, 2^2 and 2^3
      and hard-coded butterflies for w=1 and w=j.

   REV:    Jan-31-00, bug fix for radix4 2d only
   REV:    Feb-14-00, tophat R*8 NxN complex test
*/

cFourierP2::cFourierP2()
{
	// nothing to do, now that xffts[*] is dynamically
	// allocated in create_fftable
	xffts = NULL;
}

cFourierP2::~cFourierP2()
{
	if (xffts) {
		for (int n=0; n<MAX_DIM2; n++) {
			if (xffts[n]) {
				if (xffts[n]->Wtable) free(xffts[n]->Wtable);
				if (xffts[n]->Stable) free(xffts[n]->Stable);
				free(xffts[n]);
			}
		}
		free(xffts);
	}
}

void cFourierP2::create_fftable (int mdim, int real2complex)
{
	// init table if it doesn't already exist
	if (!xffts) xffts = NNW_(MAX_DIM2,FFTable);
	
	// this entry already exist?
	if (xffts[mdim]) return;
	
	// new computed entry will be at index mdim
	if (BUG) fprintf(stderr,"Creating FFTables for 2^%d ...\n",mdim);
	FFTable F = NNW_(1,FFTables);
	xffts[mdim] = F;
	
	// radix, etc.
	int n2radx, n2;
	double Q, *W, *U;

	// size of fft 
	int npts = 1<<mdim;
	if (real2complex) {
		mdim--;
		npts>>=1;
	}
	n2   = npts>>1;
	Q    = PI/n2;

	// dimension
	F->mdim=mdim;
	F->npts=npts;
	F->real2complex=real2complex;

	// Radix of fft
	if (!(mdim%3))
		n2radx=3;
	else if (!(mdim%2))
		n2radx=2;
	else
		n2radx=1;
	F->n2radx=n2radx;

	// allocate cosine table
	F->Wtable = NNW_(2*npts,double);

	// create cosine table
	W=U=F->Wtable;
	*W++=1;
	*W++=0;
	for (int i=1; i<n2; i++) {
		*W++=cos(i*Q);
		*W++=-sin(i*Q);
	}
	for (int i=0; i<npts; i++) *W++=-*U++;

	// allocate npts/2 table (real-to-complex)
	if (real2complex) {
		double QQ=Q/2, *Z;
		Z=F->Qtable=NNW_(npts,double);
		*Z++=1;
		*Z++=0;
		for (int i=1; i<n2; i++) {
			*Z++=cos(i*QQ);
			*Z++=-sin(i*QQ);
		}
	}

	// allocate swap table
	F->Stable=NNW_(npts,int);

	// Digit reverse counter
	int ii1=(1<<n2radx)-1;
	int L1=2*npts>>n2radx;
	int L2=2*npts-4;
	int ii0, jj0, ii2, ix;
	for (ii2=ii0=jj0=0; ii0<=L2; ii0+=2,jj0+=ix) {
		if (ii0<jj0) F->Stable[ii0/2]=jj0/2;
		for (ix=L1; (ix*ii1-jj0)<2; ix>>=n2radx) jj0-=ix*ii1;
	}
} 

// vector 1D fft
void cFourierP2::fft248vx ( buffer *Re, int mdim, int inc, int nvec, int incv, int isign)
{
    buffer *Im = Re+1;
    double r1, r2, r3, r4, s1, s2, s3, s4;
    double r0, r5, r6, r7, s0, s5, s6, s7, t0, t1, t2;
    int n, n0, ii, jj, LL, L1, L2, ix, ii0, ii1, ii2, ii3;
    int ivec, inc2, incv2, nradix, quit, jj0, ii4, ii5, ii6, ii7;
    double co1, co2, co3, si1, si2, si3;
    double co4, co5, co6, co7, si4, si5, si6, si7;
    
/* Pre-computed fft tables */
    create_fftable(mdim,0);

    double *Wtable;
    int *Stable;
    Wtable=xffts[mdim]->Wtable;
    Stable=xffts[mdim]->Stable;
    n=xffts[mdim]->npts;
    nradix=1<<(xffts[mdim]->n2radx);

/* complex buffer increments */
    incv2 = incv<<1;
    inc2  = inc<<1;
    L2    =  n*inc2;
    LL    =  nvec*incv2;

/* conjugate */
    if (isign > 0)
	for (ivec=0; ivec<LL; ivec+=incv2)
	    for (ii=0; ii<L2; ii+=inc2)
		Im[ii+ivec]=-Im[ii+ivec];

/* main fft loop */
    for (n0=1,L1=n*inc2,L2=L1/nradix; L1>inc2; L1=L2,L2/=nradix) {
      quit=n0*L1;

/* special w=1 butterfly */
      if (nradix==8) {
	for (ii=0; ii<quit; ii+=L1) {
	  for (ivec=0; ivec<LL; ivec+=incv2) {
	    ii0=ii+ivec;
	    ii1=ii0+L2;
	    ii2=ii1+L2;
	    ii3=ii2+L2;
	    ii4=ii3+L2;
	    ii5=ii4+L2;
	    ii6=ii5+L2;
	    ii7=ii6+L2;
	    r0=Re[ii0]+Re[ii4];
	    r4=Re[ii0]-Re[ii4];
	    r1=Re[ii1]+Re[ii5];
	    r5=Re[ii1]-Re[ii5];
	    r2=Re[ii2]+Re[ii6];
	    r6=Re[ii2]-Re[ii6];
	    r3=Re[ii3]+Re[ii7];
	    r7=Re[ii3]-Re[ii7];
	    t0=r0-r2;
	    r0+=r2;
	    r2=r1-r3;
	    r1+=r3;
	    s0=Im[ii0]+Im[ii4];
	    s4=Im[ii0]-Im[ii4];
	    s1=Im[ii1]+Im[ii5];
	    s5=Im[ii1]-Im[ii5];
	    s2=Im[ii2]+Im[ii6];
	    s6=Im[ii2]-Im[ii6];
	    s3=Im[ii3]+Im[ii7];
	    s7=Im[ii3]-Im[ii7];
	    t1=s0-s2;
	    s0+=s2;
	    s2=s1-s3;
	    s1+=s3;
	    Re[ii0]=r0+r1;
	    Re[ii4]=r0-r1;
	    Re[ii2]=t0+s2;
	    Re[ii6]=t0-s2;
	    Im[ii0]=s0+s1;
	    Im[ii4]=s0-s1;
	    Im[ii2]=t1-r2;
	    Im[ii6]=t1+r2;
	    r0=(r5-r7)*croot;
	    r5=(r5+r7)*croot;
	    s0=(s5-s7)*croot;
	    s5=(s5+s7)*croot;
	    t0=r4-r0;
	    r4+=r0;
	    r7=r6-r5;
	    r6+=r5;
	    t1=s4-s0;
	    s4+=s0;
	    s7=s6-s5;
	    s6+=s5;
	    Re[ii1]=r4+s6;
	    Re[ii7]=r4-s6;
	    Im[ii7]=s4+r6;
	    Im[ii1]=s4-r6;
	    Re[ii5]=t0+s7;
	    Re[ii3]=t0-s7;
	    Im[ii3]=t1+r7;
	    Im[ii5]=t1-r7;
	  }
	}

/* General butterfly */
	for (ix=0,jj=inc2; jj<L2; jj+=inc2) {
	  ii=jj;
	  ix+=2*n0;
	  co1=Wtable[ix];
	  si1=Wtable[ix+1];
	  ii1=2*ix;
	  co2=Wtable[ii1];
	  si2=Wtable[ii1+1];
	  ii1+=ix;
	  co3=Wtable[ii1];
	  si3=Wtable[ii1+1];
	  ii1+=ix;
	  co4=Wtable[ii1];
	  si4=Wtable[ii1+1];
	  ii1+=ix;
	  co5=Wtable[ii1];
	  si5=Wtable[ii1+1];
	  ii1+=ix;
	  co6=Wtable[ii1];
	  si6=Wtable[ii1+1];
	  ii1+=ix;
	  co7=Wtable[ii1];
	  si7=Wtable[ii1+1];

/* Common butterflies */
	  for (quit=ii+n0*L1; ii<quit; ii+=L1)
	    for (ivec=0; ivec<LL; ivec+=incv2) {
	      ii0=ii+ivec;
	      ii1=ii0+L2;
	      ii2=ii1+L2;
	      ii3=ii2+L2;
	      ii4=ii3+L2;
	      ii5=ii4+L2;
	      ii6=ii5+L2;
	      ii7=ii6+L2;
	      r0=Re[ii0]+Re[ii4];
	      r4=Re[ii0]-Re[ii4];
	      r1=Re[ii1]+Re[ii5];
	      r5=Re[ii1]-Re[ii5];
	      r2=Re[ii2]+Re[ii6];
	      r6=Re[ii2]-Re[ii6];
	      r3=Re[ii3]+Re[ii7];
	      r7=Re[ii3]-Re[ii7];
	      t0=r0-r2;
	      r0+=r2;
	      r2=r1-r3;
	      r1+=r3;
	      Re[ii0]=r0+r1;
	      r1=r0-r1;
	      s0=Im[ii0]+Im[ii4];
	      s4=Im[ii0]-Im[ii4];
	      s1=Im[ii1]+Im[ii5];
	      s5=Im[ii1]-Im[ii5];
	      s2=Im[ii2]+Im[ii6];
	      s6=Im[ii2]-Im[ii6];
	      s3=Im[ii3]+Im[ii7];
	      s7=Im[ii3]-Im[ii7];
	      t1=s0-s2;
	      s0+=s2;
	      s2=s1-s3;
	      s1+=s3;
	      Im[ii0]=s0+s1;
	      s1=s0-s1;
	      r0=t0+s2;
	      t0-=s2;
	      s0=t1-r2;
	      t1+=r2;
	      Re[ii4]=co4*r1-si4*s1;
	      Im[ii4]=co4*s1+si4*r1;
	      Re[ii2]=co2*r0-si2*s0;
	      Im[ii2]=co2*s0+si2*r0;
	      Re[ii6]=co6*t0-si6*t1;
	      Im[ii6]=co6*t1+si6*t0;
	      r0=(r5-r7)*croot;
	      r5=(r5+r7)*croot;
	      s0=(s5-s7)*croot;
	      s5=(s5+s7)*croot;
	      t0=r4-r0;
	      r4+=r0;
	      r7=r6-r5;
	      r6+=r5;
	      t1=s4-s0;
	      s4+=s0;
	      s7=s6-s5;
	      s6+=s5;
	      r0=r4+s6;
	      r4-=s6;
	      r5=t0+s7;
	      t0-=s7;
	      s0=s4-r6;
	      s4+=r6;
	      s5=t1-r7;
	      t1+=r7;
	      Re[ii1]=co1*r0-si1*s0;
	      Im[ii1]=co1*s0+si1*r0;
	      Re[ii7]=co7*r4-si7*s4;
	      Im[ii7]=co7*s4+si7*r4;
	      Re[ii5]=co5*r5-si5*s5;
	      Im[ii5]=co5*s5+si5*r5;
	      Re[ii3]=co3*t0-si3*t1;
	      Im[ii3]=co3*t1+si3*t0;
	    }
	}
      } else if (nradix==4) {

/* special w=1 butterfly */
	for (ii=0; ii<quit; ii+=L1) {
	  for (ivec=0; ivec<LL; ivec+=incv2) {
	    ii0=ii+ivec;
	    ii1=ii0+L2;
	    ii2=ii1+L2;
	    ii3=ii2+L2;
	    r1=Re[ii0]+Re[ii2];
	    r3=Re[ii0]-Re[ii2];
	    s1=Im[ii0]+Im[ii2];
	    s3=Im[ii0]-Im[ii2];
	    r2=Re[ii1]+Re[ii3];
	    r4=Re[ii1]-Re[ii3];
	    s2=Im[ii1]+Im[ii3];
	    s4=Im[ii1]-Im[ii3];
	    Re[ii0]=r1+r2;
	    Re[ii2]=r1-r2;
	    Re[ii3]=r3-s4;
	    Re[ii1]=r3+s4;
	    Im[ii0]=s1+s2;
	    Im[ii2]=s1-s2;
	    Im[ii3]=s3+r4;
	    Im[ii1]=s3-r4;
	  }
	}

/* General butterfly */
	for (ix=0, jj=inc2; jj<L2; jj+=inc2) {
	  ii=jj;
	  ix+=2*n0;

/* special w=j butterfly */
	  if (jj==(L2>>1)) {
	    for (quit=ii+n0*L1; ii<quit; ii+=L1) {
	      for (ivec=0; ivec<LL; ivec+=incv2) {
		ii0=ii+ivec;
		ii1=ii0+L2;
		ii2=ii1+L2;
		ii3=ii2+L2;
		r1=Re[ii0]+Re[ii2];
		r3=Re[ii0]-Re[ii2];
		s1=Im[ii0]+Im[ii2];
		s3=Im[ii0]-Im[ii2];
		r2=Re[ii1]+Re[ii3];
		r4=Re[ii1]-Re[ii3];
		s2=Im[ii1]+Im[ii3];
		s4=Im[ii1]-Im[ii3];
		Re[ii0]=r1+r2;
		Im[ii2]=r2-r1;
		Im[ii0]=s1+s2;
		Re[ii2]=s1-s2;
		r1=r3-s4;
		r3+=s4;
		s1=s3+r4;
		s3-=r4;
		Re[ii1]=(s3+r3)*croot;
		Im[ii1]=(s3-r3)*croot;
		Re[ii3]=(s1-r1)*croot;
		Im[ii3]=-(s1+r1)*croot;
	      }
	    }
	  } else {
	    co1=Wtable[ix];
	    si1=Wtable[ix+1];
	    ii1=ix<<1;
	    co2=Wtable[ii1];
	    si2=Wtable[ii1+1];
	    ii1+=ix;
	    co3=Wtable[ii1];
	    si3=Wtable[ii1+1];

/* Common Butterflies */
	    for (quit=ii+L1*n0; ii<quit; ii+=L1) {
	      for (ivec=0; ivec<LL; ivec+=incv2) {
		ii0=ii+ivec;
		ii1=ii0+L2;
		ii2=ii1+L2;
		ii3=ii2+L2;
		r1=Re[ii0]+Re[ii2];
		r3=Re[ii0]-Re[ii2];
		s1=Im[ii0]+Im[ii2];
		s3=Im[ii0]-Im[ii2];
		r2=Re[ii1]+Re[ii3];
		r4=Re[ii1]-Re[ii3];
		s2=Im[ii1]+Im[ii3];
		s4=Im[ii1]-Im[ii3];
		Re[ii0]=r1+r2;
		r2=r1-r2;
		r1=r3-s4;
		r3+=s4;
		Im[ii0]=s1+s2;
		s2=s1-s2;
		s1=s3+r4;
		s3-=r4;
		Re[ii1]=co1*r3-si1*s3;
		Im[ii1]=co1*s3+si1*r3;
		Re[ii2]=co2*r2-si2*s2;
		Im[ii2]=co2*s2+si2*r2;
		Re[ii3]=co3*r1-si3*s1;
		Im[ii3]=co3*s1+si3*r1;
	      }
	    }
	  }
	}
      } else {

/* nradix=2 special w=1 butterfly */
	for (ii=0; ii<quit; ii+=L1) {
	  for (ivec=0; ivec<LL; ivec+=incv2) {
	    ii0=ii+ivec;
	    ii1=ii0+L2;
	    t1=Re[ii0]-Re[ii1];
	    Re[ii0]+=Re[ii1];
	    Re[ii1]=t1;
	    t1=Im[ii0]-Im[ii1];
	    Im[ii0]+=Im[ii1];
	    Im[ii1]=t1;
	  }
	}

/* General butterfly */
	for (ix=0, jj=inc2; jj<L2; jj+=inc2) {
	  ii=jj;
	  ix+=2*n0;

/* Special w=j butterfly */
	  if (jj==(L2>>1)) {
	    for (quit=ii+n0*L1; ii<quit; ii+=L1) {
	      for (ivec=0; ivec<LL; ivec+=incv2) {
		ii0=ii+ivec;
		ii1=ii0+L2;
		t1=Re[ii1]-Re[ii0];
		Re[ii0]+=Re[ii1];
		t2=Im[ii0]-Im[ii1];
		Im[ii0]+=Im[ii1];
		Re[ii1]=t2;
		Im[ii1]=t1;
	      }
	    }
	  } else {

/* Common butterflies */
	    co1=Wtable[ix];
	    si1=Wtable[ix+1];
	    for (quit=ii+n0*L1; ii<quit; ii+=L1) {
	      for (ivec=0; ivec<LL; ivec+=incv2) {
		ii0=ii+ivec;
		ii1=ii0+L2;
		t1=Re[ii0]-Re[ii1];
		Re[ii0]+=Re[ii1];
		t2=Im[ii0]-Im[ii1];
		Im[ii0]+=Im[ii1];
		Re[ii1]=t1*co1-t2*si1;
		Im[ii1]=t1*si1+t2*co1;
	      }
	    }
	  }
	}
      }

/* For any radix */
      n0*=nradix;
    }

/* swap table? */
    if (Stable) {
      for (ix=0; ix<n; ix++)
	if (Stable[ix]) {
	  ii0=inc2*ix; jj0=inc2*Stable[ix];
	  for (ii1=ii0+LL; ii0<ii1; ii0+=incv2,jj0+=incv2) {
	    t1=Re[jj0]; Re[jj0]=Re[ii0]; Re[ii0]=t1;
	    t1=Im[jj0]; Im[jj0]=Im[ii0]; Im[ii0]=t1;
	  }
	}
    } else {

/* Digit reverse counter */
      L1=inc2*n/nradix;
      L2=inc2*(n-2);
      n0=nradix-1;
      for (ii0=jj0=0; ii0<=L2; ii0+=inc2,jj0+=ix) {
	if (ii0<jj0) {
	  for (ivec=0; ivec<LL; ivec+=incv2) {
	    ii=ii0+ivec; jj=jj0+ivec;
	    r1=Re[jj]; Re[jj]=Re[ii]; Re[ii]=r1;
	    r1=Im[jj]; Im[jj]=Im[ii]; Im[ii]=r1;
	  }
	}
	for (ix=L1; ix*n0-jj0<inc2; ix/=nradix) jj0-=ix*n0;
      }
    }

/* conjugate */
    if (isign > 0)
	for (ivec=0; ivec<LL; ivec+=incv2)
	    for (ii=0; ii<L2; ii+=inc2)
		Im[ii+ivec]=-Im[ii+ivec];
}

void cFourierP2::fft248xy (buffer *Re, int ndim, int mdim, int isign)
{
	int ii, ns, N=1<<ndim, M=1<<mdim;
	buffer *Im = Re+1;

	if (isign>0) for (ns=2*N*M, ii=0; ii<ns; ii+=2) Im[ii]=-Im[ii];
	fft248vx (Re,ndim,1,M,N,-1);
	fft248vx (Re,mdim,N,N,1,-1);
	if (isign>0) for (ns=2*N*M, ii=0; ii<ns; ii+=2) Im[ii]=-Im[ii];
}
