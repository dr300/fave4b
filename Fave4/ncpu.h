/////////////////////////////////////////////////////////////////////////////
// Fractal.cpp : implementation file
// cFractal
/////////////////////////////////////////////////////////////////////////////

#ifndef INC_FRACTAL_H
#define INC_FRACTAL_H

#include "TerryR3.h"
#include "semx.h"

#ifndef NBCPUS
#define NBCPUS 12		// some threads won't do muuch - need plenty of them
#endif

// each processor in multithread task gets it own
// specific set of parameters ... all located here:
typedef struct _blockProc {
	
	// some global info: CPU# and #elements
	int icpu;
	int npts;
	int kill;
	
	// linked list, and go/stop semaphores
	SEM_ID incoming;
	SEM_ID processed;
	struct _blockProc *next;

	// define the domain portion & processing stage to use
	int ijmin, ijmax;
	void* tDncProcessor ( void *arg );
	void (*DivideAndConquer)(int,int);
	
} blockProc;

class cNcpu
{   
	
public:
	
	cNcpu();
	virtual ~cNcpu();
	
	// user interface to compute a fractal
	void computeFractal ( int nx, int ny, int nz, buffer *pixel, big_t xa, big_t ya, big_t dx, big_t dy );
	
private:
	
// parameter space provided by user
	int i_itotal;		// size of index space
	buffer *i_xbuf;	// output buffer

	int i_nx, i_ny, i_nz;
	big_t i_xa, i_ya, i_dx, i_dy;
	
	// subset method
	void computeSubSet ( int imin, int imax );
	
private:

	blockProc *Processors;
	void execFullSet();
	
};

#endif


