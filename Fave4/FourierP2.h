/////////////////////////////////////////////////////////////////////////////
// FourierP2.cpp : header file
// cFourierP2
/////////////////////////////////////////////////////////////////////////////

#ifndef INC_FOURIER_H
#define INC_FOURIER_H

#define croot 0.70710678118654746172
#define MAX_DIM2 32	// 2^32 transforms supported

// pre-computed cosine and swap tables
typedef struct {

	// dimensions and log2(radix) {2^1, 2^2, 2^3 hardcoded}
	int mdim;          // log2(npts)
	int npts;          // npts=2^mdim
	int n2radx;        // 2^n2radx is the radix

	// table space for cosine tables (c2c & c2r), swap table
	double *Wtable;    // cosine table for fft248zx
	double *Qtable;    // cosine table for fft248zr
	int *Stable;       // swap table

	int real2complex;  // set for real-to-half-complex
	int split;         // set for split arrays, instead of interleaved

} FFTables, *FFTable;

class cFourierP2
{   
	
public:
	
	cFourierP2();
	virtual ~cFourierP2();
	
	// 2D FourierP2 transform 2^ndim x 2^mdim
	void fft248xy (buffer *Re, int ndim, int mdim, int isign);
	
private:
	
	// internal tables indexed by mdim
	FFTable *xffts;
	void create_fftable (int mdim, int real2complex);
	
	// 1D vector fft
	void fft248vx ( buffer *Re, int mdim, int inc, int nvec, int incv, int isign);
	
};

#endif


