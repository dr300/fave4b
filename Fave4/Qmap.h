/////////////////////////////////////////////////////////////////////////////
// FourierP2.cpp : implementation file
// cFourierP2
/////////////////////////////////////////////////////////////////////////////

#ifndef INC_QMAP_H
#define INC_QMAP_H

#include "FourierP2.h"

typedef struct { buffer re, im; } bcomplex;

class cQmap
{   
	
public:
	
	cQmap();
	virtual ~cQmap();
	
	// returns the Q-metric
	buffer Q_pow2 ( buffer *xbuf, int mdim, int ndim );
	
	// compute qmap as well
	buffer Qmap_pow2 ( buffer *xbuf, int mdim, int ndim );
	buffer *qmap;	// auto-allocated unless otherwise set
	
	// optional
	void setQmap ( buffer *q ) { qmap=q; }
	buffer *getQmap ( ) { return qmap; }
	
	// locates roi of highest information content, given roi[2] x roi[3]
	buffer findQroi3 ( int *roi );
	
	// optionally sets q
	void setKsize ( buffer k ) { KSIZE5=k; printf("... Qmetric-blur=%g\n",k); }

	// plots qmap
	void plotLogQmap ( char *fname, int norders );
	
private:
	
	int border, bstepz;	// used in findQroi3
	cFourierP2 *m_pFourierP2;
	
	// used to compute Qmap
	bcomplex *work;
	buffer *kfft;
	buffer KSIZE5;
	int knx, kny;
	double vnorm;
	
	// trig tables
	buffer *laplace_trig ( int nx, int ny );
	buffer* gaussianComplex2_pow2 ( bcomplex *kbuf, int mdim, int ndim, buffer kpixels );
	
	// in-place quandrant swap
	void put2 (buffer *a, int i, buffer *b, int j) { a[2*i]=b[2*j]; a[2*i+1]=b[2*j+1]; }
	void qswitch1d (buffer *c, int n, int inc);
	void qswitch2d (buffer *c, int nx, int ny);
	
	// sums ROI'd Qmetric from internal qmap
	buffer vQsumX ( int *roi );

};

#endif


