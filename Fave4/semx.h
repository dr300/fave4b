#ifndef _SEMX_H
#define _SEMX_H

// counting semaphore emulator

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef USE_WX_THREADS

#include <wx/thread.h>

// Based on code from here: http://stackoverflow.com/questions/2953071/threads-in-wxwidgets
class wxThreadWrapper : public wxThread
{
   private:
      void* (*proc)(void*);
      void *arg;
   public:
      wxThreadWrapper(void* (* m_proc)(void*), void *m_arg) :
         wxThread(wxTHREAD_DETACHED) /* Call the parent class's constructor */
   {
      // Set the appropriate function pointers & arguments
      proc = m_proc;
      arg = m_arg;
      // Attempt to create the thread, and run it if we can
      if(Create() == wxTHREAD_NO_ERROR) {
         Run();
      }
   }
   protected:
      virtual ExitCode Entry()
      {
         (*proc)(arg);
         /*while(!TestDestroy() && MoreWorkToDo()) {
            DoSaidWork();
         }*/
         return static_cast<ExitCode>(NULL);
      }
};
typedef wxThreadWrapper* pthread_t;

#else
#include <pthread.h>
#endif

// opaque structure defined in semx.cc
typedef struct _psem_t* SEM_ID;

// basic counting/binary semaphores, with P & V
SEM_ID semCCreate(int);
SEM_ID semBCreate(int);
int semGive(SEM_ID p);
int semTake(SEM_ID p);

// utilities
int semCount(SEM_ID p);
int semClear(SEM_ID p);

// thread launcher
unsigned long tLaunch (void *proc(void*), void *arg);

void initialize_threads();

#endif
